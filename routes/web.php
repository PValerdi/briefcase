<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('home');
});


Route::get('skills', function () {
    return view('skills');
});


Route::get('briefcase', function () {
    return view('briefcase');
});


Route::get('contact', function () {
    return view('contact');
});

Route::get('trajectory', function () {
    return view('trajectory');
});

Route::post('messages', function (){
    //Enviar Correo
    $data = request()->all();
    Mail::send("emails.message", $data, function($message) use($data){
        $message->from($data['email'], $data['name'])
        ->to('valerdi.300@gmail.com', 'Pedro Valerdi')
        ->subject("Contacto desde tu Sitio Web");
    });

    //Responder al usuario
    return back()->with('flash', $data['name'].' tu mensaje ha sido recibido');
})->name('messages');