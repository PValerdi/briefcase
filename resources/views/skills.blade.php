@extends('layout')

@section('content')
<!-- Start Service -->
<section id="my-service" class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-title">
							<h1><span></span> Servicios<i class="fa fa-rocket"></i></h1>
							<p>Páginas web innovadoras y programación a medida de sus necesidades</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.4s">
						<!-- Single Service -->
						<div class="single-service active">
							<i class="fa fa-html5"></i>
							<h2>Diseño Web</h2>
							<p>El diseño web consiste en la planificación de la estructura de todas las páginas, estableciendo la información que aparecerá en cada una de ellas, y creando una imagen gráfica atractiva para el usuario y útil para la presentación de la información.</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.6s">
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-code"></i>
							<h2>Programación Web</h2>
							<p>Creación de software web a la medida de las necesidades de su empresa. Realización del análisis de requisitos, creación de la base de datos, diseño de pantallas de gestión cómoda e intuitiva. Con los más altos niveles de seguridad.</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.8s">
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-mobile"></i>
							<h2>Diseño Responsivo</h2>
							<p>El diseño responsivo consiste en un diseño que responde al tamaño del dispositivo desde el que se esté visualizando el sitio web, adaptando dimensiones y ubicación del contenido para mostrar todos los elementos de una forma ordenada. </p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="1s">
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-pencil-square-o"></i>
							<h2>Administración de Sitios Web</h2>
							<p>La administración de sitios web se refiere a ser el  responsable de tus sitios web que ya se encuentran en internet. Me asegurare de que la información de tu sitio web es correcta, segura y está actualizada. </p>
						</div>
						<!-- End Single Service -->
					</div>
				</div>
			</div>
		</section>
		<!--/ End Service -->
@stop