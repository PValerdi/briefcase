@extends('layout')

@section('content')
		<!-- Start Personal Area -->
		<section id="personal-area">
			<div class="personal-main">
				<div class="personal-single">
					<div class="container">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<!-- Personal Text -->
								<div class="personal-text">
									<div class="my-info">
                                        <h1>Mí Nombre es Pedro Valerdi</h1>
                                        <h2>Programador Egresado de la Universidad Tecnológica de Puebla</h2>
										<h2 class="cd-headline clip is-full-width">
										   Con Conocimientos en 
										   <span class="cd-words-wrapper">
											   <b class="is-visible">Diseño Web</b>
											   <b>Desarrollo Web</b>
											   <b>Administración de BD</b>
											</span>
										</h2>

									</div>
								</div>
								<!--/ End Personal Text -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Personal Area -->

		<!-- Start About Me -->
		<section id="about-me" class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 fix">
						<!-- About Tab -->
						<div class="tabs-main">
							<!-- Tab Menu -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><span class="tooltips">Acerca de mi </span><a href="#welcome" role="tab" data-toggle="tab"><i class="fa fa-user"></i></a></li>
								<li role="presentation"><span class="tooltips">Habilidades</span><a href="#skill" role="tab"  data-toggle="tab"><i class="fa fa-rocket"></i></a></li>
							</ul>
							<!--/ End Tab Menu -->
							<div class="tab-content">
								<!-- Single Tab -->
								<div role="tabpanel" class="tab-pane fade in active" id="welcome">
									<div class="about-text">
										<h2 class="tab-title">Acerca de mi</h2>
										<div class="row">
											<div class="col-md-4 col-sm-4 col-xs-12">
												<!-- About Image -->
												<div class="single-image">
													<img src="images/code6.jpg" alt="">
												</div>
											</div>
											<!-- End About Image -->
											<div class="col-md-8 col-sm-12 col-xs-12">
												<div class="content">
                                                    <p>¡Bienvenido a mi sitio web!</p>
                                                    <p>Soy Pedro Valerdi un programador web, egresado de la Universidad Tecnológica de Puebla con 2  años de experiencia en todos los ámbitos del diseño de páginas web, incluyendo diseño web personalizado, desarrollo y programación de aplicaciones de gestión y portales web, creación de tiendas virtuales y desarrollo de software a la medida.</p>
                                                    <p>Pertenezco a un equipo de profesionales de desarrollo web que ofrecemos soluciones a todas las empresas que desean tener presencia en internet o necesitan un software a la medida.</p>
                                                    <p>También me desarrollo como profesional independiente (freelance), cuento con  los conocimientos y experiencia necesaria para  ofrecerte un diseño web de alta calidad y basado en buenas prácticas, desarrollo de aplicaciones web a la medida y/o creación y administración de bases de datos normalizadas, relacionales o no relacionales.</p>
												</div>
												<div class="social">
													<ul>
														<li><a href="https://www.facebook.com/pedro.valerdi.98"><i class="fa fa-facebook"></i></a><li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--/ End Single Tab -->
								<!-- Single Tab -->
								<div role="tabpanel" class="tab-pane fade" id="skill">
									<h2 class="tab-title">Mis Habilidades</h2>
									<div class="row">				
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>Diseño Web</h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"><span>60%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>Desarrollo Web</h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;"><span>65%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>PHP</h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"><span>50%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>Laravel</h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width: 55%;"><span>55%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>HTML5/CSS3</h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"><span>80%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>JavaScript</h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;"><span>75%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>Angular</h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;"><span>65%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>jQuery</h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width: 55%;"><span>55%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>Bootstrap </h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"><span>80%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
										</div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>C# / .Net</h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;"><span>65%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>My SQL</h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;"><span>65%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Skill -->
											<div class="single-skill">
												<div class="skill-info">
													<h4>SQL Server </h4>
												</div>
												<div class="progress">
												  <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"><span>80%</span></div>
												</div>
											</div>
											<!--/ End Single Skill -->
										</div>
									</div>
								</div>
								<!--/ End Single Tab -->


							</div>
						</div>
						<!--/ End About Tab -->
					</div>
				</div>
			</div>
		</section>		
		<!--/ End About Me -->	


@stop