@extends('layout')

@section('content')
		<!-- Start Contact -->
		<section id="contact" class="section">
			<div class="container">
				<div class="row">
                    @if(session()->has('flash'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                 {{ session()->get('flash')}}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-title">
                            <h1><span>Contactame</span><i class="fa fa-star"></i></h1>
                            <p>Si estas interesado en mis servicios o en que me integre a tu equipo de trabajo no dudes en contactarme y te responderé a la brevedad.</p>
                        </div>
					</div>
				</div>
				<div class="row">
					<!-- Contact Form -->
					<div class="col-md-6 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
						<form class="form" method="post" action="{{ route('messages')}}">
                            {{ csrf_field() }}
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="name" placeholder="Tu Nombre" required="required">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="email" name="email" placeholder="Tu Email" required="required">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<textarea name="body" rows="6" placeholder="Mensaje" ></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group button">	
										<button type="submit" class="button primary"><i class="fa fa-send"></i>Enviar</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<!--/ End Contact Form -->
					<!-- Contact Address -->
					<div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight" data-wow-delay="0.8s">
						<div class="contact">
							<!-- Single Address -->
							<div class="single-address">
								<i class="fa fa-phone"></i> 
								<div class="title">
									<h4>Teléfono</h4>
									<p>+52-222-469-3428,<br></p>
								</div>
							</div>
							<!--/ End Single Address -->
							<!-- Single Address -->
							<div class="single-address">
								<i class="fa fa-envelope"></i> 
								<div class="title">
									<h4>Email</h4>
									<p>valerdi.300@gmail.com,<br></p>
								</div>
							</div>
							<!--/ End Single Address -->
							<!-- Single Address -->
							<!--<div class="single-address">
								<i class="fa fa-map"></i> 
								<div class="title">
									<h4>My Location</h4>
									<p>24/8 Dokkhin Khan, <br>Uttara Dhaka Bangladesh</p>
								</div>
							</div>-->
							<!--/ End Single Address -->
						</div>
					</div>
					<!--/ End Contact Address -->
				</div>
			</div>
		</section>
		<!--/ End Contact -->
@stop